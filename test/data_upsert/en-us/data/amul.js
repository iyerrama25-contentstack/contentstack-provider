module.exports = [
  {
    "_data": {
      "title": "One updated",
      "new_updated_key": true,
      "url": "/one",
      "unique_key": "This key is only present in 'one'",
      "multiple_values": [
        1,
        2,
        3
      ],
      "age": 10,
      "normal_reference": {
        "values": ["bltd7ef93a874881c19"],
        "_content_type_id": "amul"
      },
      "number": 12,
      "reference_4": {
        "values": [],
        "_content_type_id": "amul"
      },
      "file": [
        {
          "uid": "blta59e957ad6f6929d",
          "filename": "ct-update-bug.png"
        }
      ],
      "group": {
        "file": []
      },
      "rich_text_editor": "The quick brown fox.",
      "singlefile": null,
      "tags": [],
      "locale": "en-us",
      "uid": "bltd7ef93a874881c19",
      "_version": 4,
      "published_at": "2017-12-26T14:23:58.590Z"
    },
    "_content_type_uid": "amul",
    "_uid": "bltd7ef93a874881c19",
    "_locale": "en-us"
  },
  {
    "_data": {
      "title": "Two updated",
      "new_updated_key": false,
      "url": "/two",
      "multiple_values": [
        5,
        7,
        11
      ],
      "age": 12,
      "normal_reference": {
        "values": [],
        "_content_type_id": "a"
      },
      "number": 13,
      "reference_4": {
        "values": [],
        "_content_type_id": "amul"
      },
      "file": [],
      "group": {
        "file": []
      },
      "rich_text_editor": "The quick brown fox jumped over the lazy dog.",
      "singlefile": null,
      "tags": [],
      "locale": "en-us",
      "uid": "bltf31cf1c0b639be6b",
      "_version": 3,
      "published_at": "2017-12-08T06:37:48.400Z"
    },
    "_content_type_uid": "amul",
    "_uid": "bltf31cf1c0b639be6b",
    "_locale": "en-us"
  },
  {
    "_data": {
      "title": "Three updated",
      "new_updated_key": false,
      "url": "/three",
      "multiple_values": [
        5,
        7,
        11
      ],
      "age": 14,
      "normal_reference": {
        "values": [],
        "_content_type_id": "a"
      },
      "number": 13,
      "reference_4": {
        "values": [],
        "_content_type_id": "amul"
      },
      "file": [],
      "group": {
        "file": []
      },
      "rich_text_editor": "The quick brown fox jumped over the lazy dog.",
      "singlefile": null,
      "tags": [],
      "locale": "en-us",
      "uid": "bltf31cf1c0b639be7b",
      "_version": 3,
      "published_at": "2017-12-08T06:37:48.401Z"
    },
    "_content_type_uid": "amul",
    "_uid": "bltf31cf1c0b639be7b",
    "_locale": "en-us"
  },
  {
    "_data": {
      "title": "Four updated",
      "new_updated_key": false,
      "url": "/four",
      "multiple_values": [
        13,
        17,
        19
      ],
      "age": 15,
      "normal_reference": {
        "values": [],
        "_content_type_id": "a"
      },
      "number": 13,
      "reference_4": {
        "values": [],
        "_content_type_id": "amul"
      },
      "file": [],
      "group": {
        "file": []
      },
      "rich_text_editor": "The quick brown fox jumped over the lazy dog.",
      "singlefile": null,
      "tags": [],
      "locale": "en-us",
      "uid": "bltf31cf1c0b639be8b",
      "_version": 3,
      "published_at": "2017-12-08T06:37:48.402Z"
    },
    "_content_type_uid": "amul",
    "_uid": "bltf31cf1c0b639be8b",
    "_locale": "en-us"
  },
  {
    "_data": {
      "title": "Five updated",
      "new_updated_key": false,
      "url": "/five",
      "multiple_values": [
        13,
        17,
        23
      ],
      "age": 12,
      "normal_reference": {
        "values": [],
        "_content_type_id": "a"
      },
      "number": 13,
      "reference_4": {
        "values": [],
        "_content_type_id": "amul"
      },
      "file": [],
      "group": {
        "file": []
      },
      "rich_text_editor": "The quick brown fox jumped over the lazy dog.",
      "singlefile": null,
      "tags": [],
      "locale": "en-us",
      "uid": "bltf31cf1c0b639be9b",
      "_version": 3,
      "published_at": "2017-12-08T06:37:48.403Z"
    },
    "_content_type_uid": "amul",
    "_uid": "bltf31cf1c0b639be9b",
    "_locale": "en-us"
  },
  {
    "_data": {
      "title": "Six updated",
      "url": "/six",
      "multiple_values": [
        13,
        17,
        23
      ],
      "markdown": "> Hello World\n![enter image description here][1]\n\n\n  [1]: /assets/blt02098004f5b1450c/stinson-respect.jpg\n",
      "normal_reference": {
        "values": [
          "blt9f4ec6564dca5b76"
        ],
        "_content_type_id": "refer_me"
      },
      "number": 15,
      "reference_4": {
        "values": [
          "blt2adc566041abdd30"
        ],
        "_content_type_id": "amul"
      },
      "file": [
        {
          "uid": "blt02098004f5b1450c",
          "filename": "stinson-respect.jpg",
          "created_at": "2017-12-02T07:58:05.803Z",
          "updated_at": "2017-12-02T07:58:05.803Z",
          "created_by": "bltd94c186015b52323d9429c90",
          "updated_by": "bltd94c186015b52323d9429c90",
          "content_type": "image/jpeg",
          "file_size": "10245",
          "tags": [],
          "url": "https://images.contentstack.io/v3/assets/blt0a3e0fa8bf2766a9/blt02098004f5b1450c/5a225d0d70c95a863a5370bd/download",
          "ACL": {},
          "is_dir": false,
          "_version": 1,
          "title": "stinson-respect.jpg",
          "force_load": false,
          "_internal_url": "/assets/blt02098004f5b1450c/stinson-respect.jpg"
        }
      ],
      "group": {
        "file": [
          {
            "uid": "blt06e2e92521da8d76",
            "created_at": "2017-11-28T11:29:15.565Z",
            "updated_at": "2017-11-28T11:29:15.565Z",
            "created_by": "bltd94c186015b52323d9429c90",
            "updated_by": "bltd94c186015b52323d9429c90",
            "content_type": "image/png",
            "file_size": "22861",
            "tags": [],
            "filename": "Togepi.png",
            "url": "https://images.contentstack.io/v3/assets/blt0a3e0fa8bf2766a9/blt06e2e92521da8d76/5a1d488b95203aab7b54f57a/download",
            "is_dir": false,
            "_version": 1,
            "title": "Togepi.png",
            "_internal_url": "/assets/blt06e2e92521da8d76/Togepi.png",
            "ACL": {},
            "force_load": false
          },
          {
            "uid": "blt634a9f7c9c06e242",
            "created_at": "2017-11-24T14:24:34.157Z",
            "updated_at": "2017-11-24T14:24:34.157Z",
            "created_by": "bltd94c186015b52323d9429c90",
            "updated_by": "bltd94c186015b52323d9429c90",
            "content_type": "image/jpeg",
            "file_size": "6695",
            "tags": [],
            "filename": "squirttle.jpg",
            "url": "https://images.contentstack.io/v3/assets/blt0a3e0fa8bf2766a9/blt634a9f7c9c06e242/5a182ba21be39137079a632e/download",
            "is_dir": false,
            "_version": 1,
            "title": "squirttle.jpg",
            "_internal_url": "/assets/blt634a9f7c9c06e242/squirttle.jpg",
            "ACL": {},
            "force_load": false
          }
        ]
      },
      "rich_text_editor": "<p><img src=\"/assets/blt5c0748fe31e26bf0/Togepi.png\" data-sys-asset-uid=\"blt5c0748fe31e26bf0\" alt=\"Togepi.png\" style=\"background-color: initial;\"></p><p><br></p><p><img src=\"/assets/blt634a9f7c9c06e242/squirttle.jpg\" data-sys-asset-uid=\"blt634a9f7c9c06e242\" alt=\"squirttle.jpg\"><br></p>",
      "singlefile": null,
      "tags": [],
      "locale": "en-us",
      "uid": "blt2adc566041abdd30",
      "created_by": "bltd94c186015b52323d9429c90",
      "updated_by": "bltd94c186015b52323d9429c90",
      "created_at": "2017-12-07T08:40:35.563Z",
      "updated_at": "2018-01-08T09:04:26.350Z",
      "_version": 2,
      "boolean": true,
      "published_at": "2018-01-08T09:04:37.874Z"
    },
    "_content_type_uid": "amul",
    "_uid": "blt2adc566041abdd30",
    "_locale": "en-us"
  }
]