module.exports = [
  {
    "_data": {
      "title": "Gamma_updated",
      "url": "/gamma",
      "reference": {
        "values": [
          "bltc882ba250794d94f"
        ],
        "_content_type_id": "refer_me"
      },
      "section": {
        "link_to_article": {
          "values": [
            "blt9bf125ae9ae687dc"
          ],
          "_content_type_id": "refer_me"
        }
      },
      "tags": [],
      "locale": "en-us",
      "uid": "blt73193531e9f909b5",
      "created_by": "bltd94c186015b52323d9429c90",
      "updated_by": "bltd94c186015b52323d9429c90",
      "created_at": "2017-12-07T14:14:29.556Z",
      "updated_at": "2017-12-07T14:14:29.556Z",
      "_version": 2,
      "published_at": "2017-12-07T14:14:37.809Z"
    },
    "_content_type_uid": "ref_references",
    "_uid": "blt73193531e9f909b5",
    "_locale": "en-us"
  },
  {
    "_data": {
      "title": "Omega_updated",
      "url": "/omega",
      "reference": {
        "values": [
          "blt9f4ec6564dca5b76"
        ],
        "_content_type_id": "refer_me"
      },
      "section": {
        "link_to_article": {
          "values": [
            "blt9f4ec6564dca5b76",
            "bltc882ba250794d94f"
          ],
          "_content_type_id": "refer_me"
        }
      },
      "tags": [],
      "locale": "en-us",
      "uid": "blt4754a85f7b68b41a",
      "created_by": "bltd94c186015b52323d9429c90",
      "updated_by": "bltd94c186015b52323d9429c90",
      "created_at": "2017-12-04T11:48:18.044Z",
      "updated_at": "2017-12-04T11:48:18.044Z",
      "_version": 2,
      "published_at": "2017-12-07T14:04:31.533Z"
    },
    "_content_type_uid": "ref_references",
    "_uid": "blt4754a85f7b68b41a",
    "_locale": "en-us"
  }
]