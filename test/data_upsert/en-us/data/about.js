module.exports = [
  {
    "_data": {
      "updated_at": "2017-08-10T05:59:11.016Z",
      "created_at": "2016-06-13T06:58:09.412Z",
      "title": "The Elastic Story_updated",
      "subtitle": "",
      "url": "/about",
      "introduction": {
        "title": "Solving World Data Problems",
        "image": {
          "uid": "bltbc410fd572ea7ce2",
          "filename": "about-elastic.png"
        },
        "abstract": "<p>We have a simple goal to solve the world's data problems with products that delight and inspire. As the company behind the popular open source projects — Elasticsearch, Kibana, Beats, and&nbsp;Logstash&nbsp;— we help people around the world do great things with their data. From stock quotes to Twitter streams, Apache logs to WordPress blogs, our products are extending what's possible with data, delivering on the promise that good things come from connecting the dots.</p>",
        "video": "//player.vimeo.com/video/121732979"
      },
      "body": "<p>\n\tFounded in 2012 in Amsterdam&nbsp;by the people behind Elasticsearch and Apache Lucene, Elastic set forth a vision that search can solve a plethora of data problems. The origins of the company start back in 2010, when Shay Banon wrote the first lines of Elasticsearch and open sourced it as a distributed search engine. With the rise of cloud computing and changes in IT infrastructure demanding requirements such as real-time search across infinite amounts of structured and unstructured data, Shay foresaw the need for a new type of software to solve today's real-world data problems. Steven Schuurman, Uri Boness, and Simon Willnauer shared in Shay's vision, joining forces to create the Elastic company we have today. Since then, the creators of Kibana, Logstash, and Beats have joined the Elastic family, rounding out a product portfolio known as the Elastic Stack, which is used by millions of developers around the world. The Elastic family unites employees across 32 countries into one coherent team, while the broader community spans across over 100 countries.\n</p>",
      "seo": {
        "title": "About Elastic · Immediate Insight from Data Matters",
        "description": "Founded in 2012 by the people behind Elasticsearch and Apache Lucene, we are the company behind Elasticsearch, Logstash, and Kibana. Learn more.",
        "keywords": "",
        "section": {
          "values": [
            "blt725b1cc8af4c5a2d"
          ],
          "_content_type_id": "section"
        }
      },
      "tags": [],
      "updated_by": "sys_blt57a423112de8a853",
      "created_by": "blte77d5feb88d17ec83bbbfa37",
      "uid": "blt51ab09995223a7bc",
      "_version": 2,
      "locale": "en-us",
      "published_at": "2017-12-18T09:04:09.288Z"
    },
    "_content_type_uid": "about",
    "_uid": "blt51ab09995223a7bc",
    "_locale": "en-us"
  },
  {
    "_data": {
      "title": "about test entry fastly condition_updated",
      "subtitle": "subtitle",
      "url": "/testing-about-again",
      "introduction": {
        "title": "test3",
        "image": null,
        "abstract": "",
        "video": ""
      },
      "body": "",
      "seo": {
        "title": "",
        "description": "",
        "keywords": "",
        "section": {
          "values": [
            "blt725b1cc8af4c5a2d"
          ],
          "_content_type_id": "section"
        }
      },
      "tags": [],
      "locale": "en-us",
      "uid": "blt606d25e9b4226891",
      "created_by": "blt0ac59771801e2eb09befe680",
      "updated_by": "sys_blt57a423112de8a853",
      "created_at": "2017-02-14T12:22:18.572Z",
      "updated_at": "2017-11-10T13:10:08.815Z",
      "_version": 2,
      "published_at": "2017-12-18T09:04:07.167Z"
    },
    "_content_type_uid": "about",
    "_uid": "blt606d25e9b4226891",
    "_locale": "en-us"
  }
]