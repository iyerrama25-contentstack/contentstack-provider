module.exports = [
  {
    "_data": {
      "created_at": "2017-08-18T11:59:58.747Z",
      "updated_at": "2017-11-23T05:35:42.740Z",
      "title": "Amul",
      "uid": "amul",
      "schema": [
        {
          "display_name": "Title",
          "uid": "title",
          "data_type": "text",
          "mandatory": true,
          "unique": true,
          "field_metadata": {
            "_default": true
          },
          "multiple": false
        },
        {
          "display_name": "URL",
          "uid": "url",
          "data_type": "text",
          "mandatory": false,
          "field_metadata": {
            "_default": true
          },
          "multiple": false,
          "unique": false
        },
        {
          "data_type": "text",
          "display_name": "Markdown",
          "uid": "markdown",
          "field_metadata": {
            "description": "",
            "markdown": true
          },
          "multiple": false,
          "mandatory": false,
          "unique": false
        },
        {
          "data_type": "reference",
          "display_name": "Normal Reference",
          "reference_to": "a",
          "field_metadata": {
            "ref_multiple": true
          },
          "uid": "normal_reference",
          "multiple": false,
          "mandatory": false,
          "unique": false
        },
        {
          "data_type": "number",
          "display_name": "Number",
          "uid": "number",
          "field_metadata": {
            "description": "",
            "default_value": 12
          },
          "multiple": false,
          "mandatory": false,
          "unique": false
        },
        {
          "data_type": "reference",
          "display_name": "Reference",
          "reference_to": "amul",
          "field_metadata": {
            "ref_multiple": true
          },
          "uid": "reference_4",
          "multiple": false,
          "mandatory": false,
          "unique": false
        },
        {
          "data_type": "file",
          "display_name": "File",
          "uid": "file",
          "extensions": [],
          "field_metadata": {
            "description": "",
            "rich_text_type": "standard"
          },
          "multiple": true,
          "mandatory": false,
          "unique": false
        },
        {
          "data_type": "group",
          "display_name": "Group",
          "field_metadata": {},
          "schema": [
            {
              "data_type": "file",
              "display_name": "File",
              "uid": "file",
              "extensions": [],
              "field_metadata": {
                "description": "",
                "rich_text_type": "standard"
              },
              "multiple": true,
              "mandatory": false,
              "unique": false
            }
          ],
          "uid": "group",
          "multiple": false,
          "mandatory": false,
          "unique": false
        },
        {
          "data_type": "text",
          "display_name": "Rich text editor",
          "uid": "rich_text_editor",
          "field_metadata": {
            "allow_rich_text": true,
            "description": "",
            "multiline": false,
            "rich_text_type": "advanced"
          },
          "multiple": false,
          "mandatory": false,
          "unique": false
        },
        {
          "data_type": "file",
          "display_name": "SingleFile",
          "uid": "singlefile",
          "extensions": [],
          "field_metadata": {
            "description": "",
            "rich_text_type": "standard"
          },
          "multiple": false,
          "mandatory": false,
          "unique": false
        }
      ],
      "options": {
        "is_page": true,
        "singleton": false,
        "title": "title",
        "sub_title": [],
        "url_pattern": "/:title",
        "url_prefix": "/"
      },
      "references": {
        "_data:normal_reference": "a",
        "_data:reference_4": "amul"
      }
    },
    "_content_type_uid": "_content_types",
    "_uid": "amul",
    "_locale": "en-us"
  },
  {
    "_data": {
      "created_at": "2017-09-07T14:17:46.398Z",
      "updated_at": "2017-12-04T11:38:46.353Z",
      "title": "Refer Me",
      "uid": "refer_me",
      "schema": [
        {
          "display_name": "Title",
          "uid": "title",
          "data_type": "text",
          "mandatory": true,
          "unique": true,
          "field_metadata": {
            "_default": true
          },
          "multiple": false
        },
        {
          "display_name": "URL",
          "uid": "url",
          "data_type": "text",
          "mandatory": false,
          "field_metadata": {
            "_default": true
          },
          "multiple": false,
          "unique": false
        },
        {
          "data_type": "text",
          "display_name": "Single line textbox",
          "uid": "single_line",
          "field_metadata": {
            "description": "",
            "default_value": "The quick brown fox jumped over the lazy dog."
          },
          "format": "",
          "error_messages": {
            "format": ""
          },
          "multiple": false,
          "mandatory": false,
          "unique": false
        },
        {
          "data_type": "text",
          "display_name": "Multi line textbox",
          "uid": "multi_line",
          "field_metadata": {
            "description": "",
            "default_value": "The quick brown fox jumped over the lazy dog.The quick brown fox jumped over the lazy dog.The quick brown fox jumped over the lazy dog.",
            "multiline": true
          },
          "format": "",
          "error_messages": {
            "format": ""
          },
          "multiple": false,
          "mandatory": false,
          "unique": false
        },
        {
          "data_type": "reference",
          "display_name": "Multi-self-reference",
          "reference_to": "refer_me",
          "field_metadata": {
            "ref_multiple": true
          },
          "uid": "multi_self_reference",
          "mandatory": false,
          "multiple": false,
          "unique": false
        },
        {
          "data_type": "text",
          "display_name": "Rich text editor",
          "uid": "rich_text_editor",
          "field_metadata": {
            "allow_rich_text": true,
            "description": "",
            "multiline": false,
            "rich_text_type": "advanced"
          },
          "multiple": true,
          "mandatory": false,
          "unique": false
        },
        {
          "data_type": "number",
          "display_name": "Number",
          "uid": "number",
          "field_metadata": {
            "description": "",
            "default_value": 500
          },
          "multiple": true,
          "mandatory": false,
          "unique": false
        },
        {
          "data_type": "boolean",
          "display_name": "Boolean",
          "uid": "boolean",
          "field_metadata": {
            "description": "",
            "default_value": true
          },
          "multiple": false,
          "mandatory": false,
          "unique": false
        },
        {
          "data_type": "isodate",
          "display_name": "Date",
          "uid": "date",
          "startDate": null,
          "endDate": null,
          "field_metadata": {
            "description": "",
            "default_value": {
              "custom": false,
              "date": "",
              "time": ""
            }
          },
          "multiple": false,
          "mandatory": false,
          "unique": false
        },
        {
          "data_type": "file",
          "display_name": "File",
          "uid": "file",
          "extensions": [],
          "field_metadata": {
            "description": "",
            "rich_text_type": "standard"
          },
          "multiple": true,
          "mandatory": false,
          "unique": false
        },
        {
          "data_type": "link",
          "display_name": "Link",
          "uid": "link",
          "field_metadata": {
            "description": "",
            "default_value": {
              "title": "Google",
              "url": "https://google.com"
            }
          },
          "multiple": false,
          "mandatory": false,
          "unique": false
        },
        {
          "data_type": "reference",
          "display_name": "Self Reference",
          "reference_to": "refer_me",
          "field_metadata": {
            "ref_multiple": false
          },
          "uid": "self_reference",
          "mandatory": false,
          "multiple": false,
          "unique": false
        },
        {
          "data_type": "reference",
          "display_name": "Ref Amul",
          "reference_to": "amul",
          "field_metadata": {
            "ref_multiple": true
          },
          "uid": "ref_amul",
          "multiple": false,
          "mandatory": false,
          "unique": false
        }
      ],
      "options": {
        "is_page": true,
        "singleton": false,
        "title": "title",
        "sub_title": [],
        "url_pattern": "/:title",
        "url_prefix": "/"
      },
      "references": {
        "_data:multi_self_reference": "refer_me",
        "_data:self_reference": "refer_me",
        "_data:ref_amul": "amul"
      }
    },
    "_content_type_uid": "_content_types",
    "_uid": "refer_me",
    "_locale": "en-us"
  },
  {
    "_data": {
      "created_at": "2017-06-30T05:45:33.186Z",
      "updated_at": "2017-12-04T11:56:25.437Z",
      "title": "Ref References",
      "uid": "ref_references",
      "schema": [
        {
          "display_name": "Title",
          "uid": "title",
          "data_type": "text",
          "mandatory": true,
          "unique": true,
          "field_metadata": {
            "_default": true
          },
          "multiple": false
        },
        {
          "display_name": "URL",
          "uid": "url",
          "data_type": "text",
          "mandatory": false,
          "field_metadata": {
            "_default": true
          },
          "multiple": false,
          "unique": false
        },
        {
          "data_type": "reference",
          "display_name": "Reference",
          "reference_to": "refer_me",
          "field_metadata": {
            "ref_multiple": true
          },
          "uid": "reference",
          "multiple": false,
          "mandatory": false,
          "unique": false
        },
        {
          "data_type": "group",
          "display_name": "section_ola",
          "field_metadata": {},
          "schema": [
            {
              "data_type": "reference",
              "display_name": "link_to_article",
              "reference_to": "refer_me",
              "field_metadata": {
                "ref_multiple": true
              },
              "uid": "link_to_article",
              "multiple": false,
              "mandatory": false,
              "unique": false
            }
          ],
          "uid": "section",
          "multiple": false,
          "mandatory": false,
          "unique": false
        }
      ],
      "options": {
        "is_page": true,
        "singleton": false,
        "title": "title",
        "sub_title": [],
        "url_pattern": "/:title",
        "url_prefix": "/"
      },
      "references": {
        "_data:reference": "refer_me",
        "_data:section:link_to_article": "refer_me"
      }
    },
    "_content_type_uid": "_content_types",
    "_uid": "ref_references",
    "_locale": "en-us"
  }
]