module.exports = [
  {
    "_data": {
      "title": "Refme3-updated",
      "url": "/refme3-updated",
      "single_line": "The quick brown fox jumped over the lazy dog.",
      "multi_line": "The quick brown fox jumped over the lazy dog.The quick brown fox jumped over the lazy dog.The quick brown fox jumped over the lazy dog.",
      "rich_text_editor": [
        "<p><span style=\"color: rgb(51, 51, 51); font-family: Lato, &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Sans&quot;, Verdana, Tahoma, sans-serif; font-size: 18px; font-variant-ligatures: none;\">Because</span><span style=\"color: rgb(51, 51, 51); font-family: Lato, &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Sans&quot;, Verdana, Tahoma, sans-serif; font-size: 18px; font-variant-ligatures: none;\">&nbsp;</span><code style=\"font-variant-ligatures: none; font-family: Monaco, Consolas, &quot;Lucida Console&quot;, monospace; line-height: 1.5em; padding: 0.1em 0.3em; font-size: 0.9em; color: rgb(4, 4, 4); background-color: rgb(242, 242, 242); border-radius: 2px;\">server.listen()</code><span style=\"color: rgb(51, 51, 51); font-family: Lato, &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Sans&quot;, Verdana, Tahoma, sans-serif; font-size: 18px; font-variant-ligatures: none;\">&nbsp;</span><span style=\"color: rgb(51, 51, 51); font-family: Lato, &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Sans&quot;, Verdana, Tahoma, sans-serif; font-size: 18px; font-variant-ligatures: none;\">hands off most of the work to the master process, there are three cases where the behavior between a normal Node.js process and a cluster worker differs:</span></p><ol style=\"margin-bottom: 0.6em; margin-left: 2em; padding-left: 0px; color: rgb(51, 51, 51); font-family: Lato, &quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, &quot;Lucida Sans&quot;, Verdana, Tahoma, sans-serif; font-size: 18px; font-variant-ligatures: none; background-color: rgb(255, 255, 255);\"><li style=\"margin-bottom: 0.5em;\"><code style=\"font-family: Monaco, Consolas, &quot;Lucida Console&quot;, monospace; line-height: 1.5em; padding: 0.1em 0.3em; font-size: 0.9em; color: rgb(4, 4, 4); background-color: rgb(242, 242, 242); border-radius: 2px;\">server.listen({fd: 7})</code>&nbsp;Because the message is passed to the master, file descriptor 7&nbsp;<span style=\"font-weight: 700;\">in the parent</span>&nbsp;will be listened on, and the handle passed to the worker, rather than listening to the worker's idea of what the number 7 file descriptor references.</li><li style=\"margin-bottom: 0.5em;\"><code style=\"font-family: Monaco, Consolas, &quot;Lucida Console&quot;, monospace; line-height: 1.5em; padding: 0.1em 0.3em; font-size: 0.9em; color: rgb(4, 4, 4); background-color: rgb(242, 242, 242); border-radius: 2px;\">server.listen(handle)</code>&nbsp;Listening on handles explicitly will cause the worker to use the supplied handle, rather than talk to the master process. If the worker already has the handle, then it's presumed that you know what you are doing.</li><li><code style=\"font-family: Monaco, Consolas, &quot;Lucida Console&quot;, monospace; line-height: 1.5em; padding: 0.1em 0.3em; font-size: 0.9em; color: rgb(4, 4, 4); background-color: rgb(242, 242, 242); border-radius: 2px;\">server.listen(0)</code>&nbsp;Normally, this will cause servers to listen on a random port. However, in a cluster, each worker will receive the same \"random\" port each time they do&nbsp;<code style=\"font-family: Monaco, Consolas, &quot;Lucida Console&quot;, monospace; line-height: 1.5em; padding: 0.1em 0.3em; font-size: 0.9em; color: rgb(4, 4, 4); background-color: rgb(242, 242, 242); border-radius: 2px;\">listen(0)</code>. In essence, the port is random the first time, but predictable thereafter. If you want to listen on a unique port, generate a port number based on the cluster worker ID.</li></ol>"
      ],
      "number": [
        500
      ],
      "boolean": true,
      "date": "2017-09-07T02:24:20.000Z",
      "file": [
        {
          "uid": "bltf79c8af27dd98b20",
          "filename": "mongo-db.png"
        }
      ],
      "link": {
        "title": "Google",
        "href": "https://google.com"
      },
      "self_reference": {
        "values": [
          "bltc882ba250794d94f"
        ],
        "_content_type_id": "refer_me"
      },
      "tags": [],
      "locale": "en-us",
      "uid": "blt9bf125ae9ae687dc",
      "created_by": "bltd94c186015b52323d9429c90",
      "updated_by": "bltd94c186015b52323d9429c90",
      "created_at": "2017-09-07T14:24:53.517Z",
      "updated_at": "2017-10-13T12:47:41.508Z",
      "_version": 5,
      "multi_self_reference": {
        "values": [
          "bltc882ba250794d94f",
          "blt9f4ec6564dca5b76",
          "blt9bf125ae9ae687dc"
        ],
        "_content_type_id": "refer_me"
      },
      "published_at": "2017-12-07T14:14:57.279Z"
    },
    "_content_type_uid": "refer_me",
    "_uid": "blt9bf125ae9ae687dc",
    "_locale": "en-us"
  },
  {
    "_data": {
      "title": "Refme2",
      "url": "/refme2",
      "single_line": "The quick brown fox jumped over the lazy dog.",
      "multi_line": "The quick brown fox jumped over the lazy dog.The quick brown fox jumped over the lazy dog.The quick brown fox jumped over the lazy dog.",
      "rich_text_editor": [
        "<h4 id=\"x11.9.3\" style=\"color: green; margin-bottom: 4px; font-size: 19.2px; font-family: sans-serif;\">11.9.3 The Abstract Equality Comparison Algorithm&nbsp;<a href=\"https://es5.github.io/#x11.9.3\">#</a>&nbsp;<a href=\"https://es5.github.io/#x11.9.3-toc\" class=\"bak\">Ⓣ</a>&nbsp;</h4><p style=\"margin-top: 4px; color: rgb(0, 0, 0); font-family: sans-serif; font-size: medium;\">The comparison&nbsp;<i>x</i>&nbsp;==&nbsp;<i>y</i>, where&nbsp;<i>x</i>&nbsp;and&nbsp;<i>y</i>&nbsp;are values, produces&nbsp;<strong>true</strong>&nbsp;or&nbsp;<strong>false</strong>. Such a comparison is performed as follows:</p><h4 id=\"x11.9.3\" style=\"color: green; margin-bottom: 4px; font-size: 19.2px; font-family: sans-serif;\">11.9.3 The Abstract Equality Comparison Algorithm&nbsp;<a href=\"https://es5.github.io/#x11.9.3\">#</a>&nbsp;<a href=\"https://es5.github.io/#x11.9.3-toc\" class=\"bak\">Ⓣ</a>&nbsp;</h4><p style=\"margin-top: 4px; color: rgb(0, 0, 0); font-family: sans-serif; font-size: medium;\">The comparison&nbsp;<i>x</i>&nbsp;==&nbsp;<i>y</i>, where&nbsp;<i>x</i>&nbsp;and&nbsp;<i>y</i>&nbsp;are values, produces&nbsp;<strong>true</strong>&nbsp;or&nbsp;<strong>false</strong>. Such a comparison is performed as follows:</p><h4 id=\"x11.9.3\" style=\"color: green; margin-bottom: 4px; font-size: 19.2px; font-family: sans-serif;\">11.9.3 The Abstract Equality Comparison Algorithm&nbsp;<a href=\"https://es5.github.io/#x11.9.3\">#</a>&nbsp;<a href=\"https://es5.github.io/#x11.9.3-toc\" class=\"bak\">Ⓣ</a>&nbsp;</h4><p style=\"margin-top: 4px; color: rgb(0, 0, 0); font-family: sans-serif; font-size: medium;\">The comparison&nbsp;<i>x</i>&nbsp;==&nbsp;<i>y</i>, where&nbsp;<i>x</i>&nbsp;and&nbsp;<i>y</i>&nbsp;are values, produces&nbsp;<strong>true</strong>&nbsp;or&nbsp;<strong>false</strong>. Such a comparison is performed as follows:</p>"
      ],
      "number": [
        500
      ],
      "boolean": true,
      "date": "2017-09-07T02:23:42.000Z",
      "file": [
        {
          "uid": "blta99ba51a986c851f",
          "filename": "roots-logo.png"
        }
      ],
      "link": {
        "title": "Google",
        "href": "https://google.com"
      },
      "self_reference": {
        "values": [],
        "_content_type_id": "refer_me"
      },
      "tags": [
        "8523266+565332.3...",
        "+5232+1",
        "35+5+5---*",
        "+653",
        "323",
        "55622665",
        "2",
        "56225",
        "136123++",
        ";'.''/"
      ],
      "locale": "en-us",
      "uid": "bltc882ba250794d94f",
      "created_by": "bltd94c186015b52323d9429c90",
      "updated_by": "bltd94c186015b52323d9429c90",
      "created_at": "2017-09-07T14:24:09.315Z",
      "updated_at": "2017-12-01T12:32:33.737Z",
      "_version": 8,
      "multi_self_reference": {
        "values": [],
        "_content_type_id": "refer_me"
      },
      "published_at": "2017-12-07T14:14:50.235Z"
    },
    "_content_type_uid": "refer_me",
    "_uid": "bltc882ba250794d94f",
    "_locale": "en-us"
  },
  {
    "_data": {
      "title": "Refme1",
      "url": "/refme1",
      "single_line": "The quick brown fox jumped over the lazy dog.",
      "multi_line": "The quick brown fox jumped over the lazy dog.The quick brown fox jumped over the lazy dog.The quick brown fox jumped over the lazy dog.",
      "rich_text_editor": [
        "<h4 id=\"x11.9.2\" style=\"color: green; margin-bottom: 4px; font-size: 19.2px; font-family: sans-serif;\">11.9.2 The Does-not-equals Operator ( != )&nbsp;<a href=\"https://es5.github.io/#x11.9.2\" style=\"color: rgb(187, 187, 187); border: none; font-weight: normal;\">#</a>&nbsp;<a href=\"https://es5.github.io/#x11.9.2-toc\" class=\"bak\" style=\"color: rgb(187, 187, 187); border: none; font-weight: normal;\">Ⓣ</a>&nbsp;</h4><p style=\"margin-top: 4px; color: rgb(0, 0, 0); font-family: sans-serif; font-size: medium;\">The production EqualityExpression&nbsp;<strong>:</strong>&nbsp;<i style=\"font-family: serif;\">EqualityExpression</i>&nbsp;<code>&lt;strong&gt;!=&lt;/strong&gt;</code>&nbsp;<i style=\"font-family: serif;\">RelationalExpression</i>&nbsp;is evaluated as follows:</p><ol style=\"color: rgb(0, 0, 0); font-family: sans-serif; font-size: medium;\"><li style=\"margin-top: 4px; margin-bottom: 4px;\">Let&nbsp;<i style=\"font-family: serif;\">lref</i>&nbsp;be the result of evaluating&nbsp;<i style=\"font-family: serif;\">EqualityExpression</i>.</li><li style=\"margin-top: 4px; margin-bottom: 4px;\">Let&nbsp;<i style=\"font-family: serif;\">lval</i>&nbsp;be&nbsp;<a href=\"https://es5.github.io/#x8.7.1\" style=\"color: rgb(184, 46, 0);\">GetValue</a>(<i style=\"font-family: serif;\">lref</i>).</li><li style=\"margin-top: 4px; margin-bottom: 4px;\">Let&nbsp;<i style=\"font-family: serif;\">rref</i>&nbsp;be the result of evaluating&nbsp;<i style=\"font-family: serif;\">RelationalExpression</i>.</li><li style=\"margin-top: 4px; margin-bottom: 4px;\">Let&nbsp;<i style=\"font-family: serif;\">rval</i>&nbsp;be&nbsp;<a href=\"https://es5.github.io/#x8.7.1\" style=\"color: rgb(184, 46, 0);\">GetValue</a>(<i style=\"font-family: serif;\">rref</i>).</li><li style=\"margin-top: 4px; margin-bottom: 4px;\">Let&nbsp;<i style=\"font-family: serif;\">r</i>&nbsp;be the result of performing abstract equality comparison&nbsp;<i style=\"font-family: serif;\">rval</i>&nbsp;==&nbsp;<i style=\"font-family: serif;\">lval</i>. (see&nbsp;<a href=\"https://es5.github.io/#x11.9.3\" style=\"color: rgb(184, 46, 0);\">11.9.3</a>).</li><li style=\"margin-top: 4px; margin-bottom: 4px;\">If&nbsp;<i style=\"font-family: serif;\">r</i>&nbsp;is&nbsp;<strong>true</strong>, return&nbsp;<strong>false</strong>. Otherwise, return&nbsp;<strong>true</strong>.</li></ol>"
      ],
      "number": [
        500
      ],
      "boolean": true,
      "date": "2017-09-07T02:20:52.000Z",
      "file": {
        "values": [],
        "_content_type_id": "_assets"
      },
      "link": {
        "title": "Google",
        "href": "https://google.com"
      },
      "self_reference": {
        "values": [
          "blt9f4ec6564dca5b76"
        ],
        "_content_type_id": "refer_me"
      },
      "tags": [],
      "locale": "en-us",
      "uid": "blt9f4ec6564dca5b76",
      "created_by": "bltd94c186015b52323d9429c90",
      "updated_by": "bltd94c186015b52323d9429c90",
      "created_at": "2017-09-07T14:21:49.396Z",
      "updated_at": "2017-12-04T11:44:39.359Z",
      "_version": 7,
      "multi_self_reference": {
        "values": [],
        "_content_type_id": "refer_me"
      },
      "ref_amul": {
        "values": [
          "blt2f1ed82ee3185030",
          "blt95b2324b8540fcb3"
        ],
        "_content_type_id": "amul"
      },
      "published_at": "2017-12-07T14:04:51.323Z"
    },
    "_content_type_uid": "refer_me",
    "_uid": "blt9f4ec6564dca5b76",
    "_locale": "en-us"
  }
]