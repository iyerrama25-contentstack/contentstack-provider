const config = require('./config');
let sync_instance = null;
class Synchronize {
	constructor () {
		if (!sync_instance) {
			console.log('inside Synchronize constructor', config.get('contentstack'));
			sync_instance = this;
		}
		return sync_instance;
	}
}

module.exports = new Synchronize();