'use strict';


let config = null;

/**
 * Based on provider, load their configurations
 */

class Config {
	/**
	 * Load default configurations
	 * @return {Number} 	: Status of config
	 */
	constructor () {
		if (!config) {
			this.languages = [
				{
					code: 'en-us',
					relative_url_prefix: '/'
				},
				{
					code: 'ja-jp',
					relative_url_prefix: '/jp/'
				}
			];
			config = this;
		}
		console.log('returning config');
		return config;
	}

	get (key) {
		return key.split('.').reduce((total, current) => {
			if (total && typeof total[current] !== 'undefined') return total[current];
			return undefined;
		}, config);
	}

	set (key, value) {
		key.split('.').reduce((parent, current, index, array) => {
			if (index < (array.length -1)) {
				return parent[current];
			} else {
				parent[current] = value;
			}
		}, config);
	}
}

module.exports = new Config();