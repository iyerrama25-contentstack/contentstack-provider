const _config = require('./config');

let child_instance = null;
module.exports = (parent, config) => {
	if (!child_instance && typeof config === 'object') {
		console.log('parent:', parent);
		class B extends parent {
			constructor (parent, config) {
				super();
				this.parent = new parent();
				for (let key in config)
					_config.set(key, config[key]);
				this.Synchronize = require('./synchronize');
				console.log('B');
			}
			methodB () {
				console.log('methodB');
			}
		}
		child_instance = new B(parent, config);
	}
	return child_instance;
}