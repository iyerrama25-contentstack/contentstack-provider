const request = require('request'),
	EventEmitter = require('events').EventEmitter;

let parent = null;

const dummy_assets = {
  assetOne: {
    _uid: "blt070cfd9c4daddb07",
    _locale: "en-us",
    _content_type_uid: "_assets",
    _data: {
      uid: "blt070cfd9c4daddb07",
      content_type: "image/jpeg",
      file_size: "10245",
      tags: [],
      filename: "articuno-custom.jpg",
      url:
        "https://images.contentstack.io/v3/assets/blt0a3e0fa8bf2766a9/blt070cfd9c4daddb07/5a182b47e796d29d7bc96251/download",
      is_dir: false,
      _version: 1,
      title: "Articuno Custom",
      _internal_url: "/assets/blt070cfd9c4daddb07/articuno-custom.jpg"
    }
  }
}

class Parent extends EventEmitter {
	constructor () {
		if (!parent) {
			super();
			this.on('startStream', (url, cb) => {
				let req_stream = request({url: url});
				return cb(req_stream);
			});
			parent = this;
		}
		return parent;
	}

	listners () {
		parent.emit('assetDownload', dummy_assets.assetOne, (cb) => {
			console.log('@assetDownload: ', cb);
		});
	}
}

module.exports = new Parent();