const DB = require('../lib/Providers');

let assets = [
	{
    "uid": "bltcbe4371f682ec346",
    "created_at": "2017-12-21T07:12:00.103Z",
    "updated_at": "2017-12-21T07:12:00.103Z",
    "created_by": "bltd94c186015b52323d9429c90",
    "updated_by": "bltd94c186015b52323d9429c90",
    "content_type": "image/png",
    "file_size": "33154",
    "tags": [],
    "filename": "Screenshot from 2017-12-15 10-47-43.png",
    "url": "https://images.contentstack.io/v3/assets/blt0a3e0fa8bf2766a9/bltcbe4371f682ec346/5a3b5ec05a988db96e45d6f1/download",
    "ACL": {},
    "is_dir": false,
    "_version": 1,
    "title": "Screenshot from 2017-12-15 10-47-43.png"
	},
  {
    "uid": "bltd5e11a36a7d266ff",
    "created_at": "2017-12-05T04:58:22.792Z",
    "updated_at": "2017-12-07T05:39:13.158Z",
    "created_by": "bltd94c186015b52323d9429c90",
    "updated_by": "bltd94c186015b52323d9429c90",
    "content_type": "image/png",
    "file_size": "45410",
    "tags": [],
    "filename": "all-clear.png",
    "url": "https://images.contentstack.io/v3/assets/blt0a3e0fa8bf2766a9/bltd5e11a36a7d266ff/5a28d401dd78edef3a9fb0d7/download",
    "ACL": {},
    "is_dir": false,
    "parent_uid": null,
    "_version": 2,
    "title": "raichu.jpg",
    "description": ""
  },
  {
    "uid": "bltd5e11a36a7d266ff",
    "created_at": "2017-12-05T04:58:22.792Z",
    "updated_at": "2017-12-07T05:39:13.158Z",
    "created_by": "bltd94c186015b52323d9429c90",
    "random_data": {
    	"_data": {
    		"title": "_pingu_",
    		"file": "something_random_"
    	}
    },
    "updated_by": "bltd94c186015b52323d9429c90",
    "content_type": "image/png",
    "file_size": "45410",
    "tags": [],
    "filename": "all-clear.png",
    "url": "https://images.contentstack.io/v3/assets/blt0a3e0fa8bf2766a9/bltd5e11a36a7d266ff/5a28d401dd78edef3a9fb0d7/download",
    "ACL": {},
    "is_dir": false,
    "parent_uid": null,
    "_version": 2,
    "title": "raichu.jpg",
    "description": ""
  },
  {
    "uid": "bltd7d6d8096eaca2b5",
    "created_at": "2017-12-02T08:09:41.645Z",
    "updated_at": "2017-12-02T08:09:41.645Z",
    "created_by": "bltd94c186015b52323d9429c90",
    "updated_by": "bltd94c186015b52323d9429c90",
    "content_type": "image/jpeg",
    "file_size": "16208",
    "tags": [],
    "filename": "iyerrama.jpg",
    "url": "https://images.contentstack.io/v3/assets/blt0a3e0fa8bf2766a9/bltd7d6d8096eaca2b5/5a225fc537175f364555b129/download",
    "ACL": {},
    "is_dir": false,
    "_version": 1,
    "title": "iyerrama.jpg"
  }
];

let entries = [
	{
		_data: {
			title: 'Alpha - One',
			uid: 'blt-alpha-one',
			body: 'The quick brown fox jumped over the lazy dog!',
			value: 1
		},
		_locale: 'en-us',
		_content_type: 'amul',
		_uid: 'blt-alpha-one'
	},
	{
		_data: {
			title: 'Alpha - Two',
			uid: 'blt-alpha-two',
			body: 'The quick brown fox jumped over the lazy dog!',
			value: 2
		},
		_locale: 'en-us',
		_content_type: 'amul',
		_uid: 'blt-alpha-two'
	},
	{
		_data: {
			title: 'Alpha - Three',
			uid: 'blt-alpha-three',
			body: 'The quick brown fox jumped over the lazy dog!',
			value: 3
		},
		_locale: 'en-us',
		_content_type: 'amul',
		_uid: 'blt-alpha-three'
	}
];

let upsert_entries = [
	{
		_data: {
			title: 'Alpha - One - Upserted',
			uid: 'blt-alpha-one',
			body: 'The quick brown fox jumped over the lazy dog!',
			value: 11
		},
		_locale: 'en-us',
		_content_type: 'amul',
		_uid: 'blt-alpha-one'
	},
	{
		_data: {
			title: 'Alpha - Two - Upserted',
			uid: 'blt-alpha-two',
			body: 'The quick brown fox jumped over the lazy dog!',
			value: 22
		},
		_locale: 'en-us',
		_content_type: 'amul',
		_uid: 'blt-alpha-two'
	},
	{
		_data: {
			title: 'Alpha - Three - Upserted',
			uid: 'blt-alpha-three',
			body: 'The quick brown fox jumped over the lazy dog!',
			value: 33
		},
		_locale: 'en-us',
		_content_type: 'amul',
		_uid: 'blt-alpha-three'
	}
];

function upsert () {
	return new Promise((resolve, reject) => {
		return DB.upsert({
			_content_type: 'alpha',
			_locale: 'en-us',
			_uid: upsert_entity1.uid,
			_data: upsert_entity1
		}).then(() => resolve()).catch(error => reject(error));
	});
}

function insert () {
	return new Promise((resolve, reject) => {
		return DB.insert({
			_content_type: 'alpha',
			_locale: 'en-us',
			_uid: entity2.uid,
			_data: entity2
		}).then(() => {
			return resolve();
		}).catch(error => {
			return reject(error);
		})
	});
}

function remove () {
	return new Promise((resolve, reject) => {
		return DB.remove({
			_content_type: 'alpha',
			_locale: 'en-us',
			_uid: entity2.uid
		}).then(() => resolve()).catch(error => reject(error));
	});
}

function find () {
	return new Promise((resolve, reject) => {
		DB.find({
			_content_type: 'alpha',
			_locale: 'en-us'
		}, {}).then(result => {
			console.log(JSON.stringify(result));
		}).catch(e => {
			console.error(e)
		});
	});
}


function findOne () {
	return new Promise((resolve, reject) => {
		DB.findOne({
			_content_type: 'alpha',
			_locale: 'en-us',
			_uid: entity2.uid
		}, {}).then(result => {
			console.log(JSON.stringify(result));
		}).catch(e => {
			console.error(e)
		});
	});
}


// remove().then(() => {
// 	console.log('remove')
// }).catch(error => {
// 	console.error(error);
// });
// 
// DB.find({
// 	_content_type: 'amul',
// 	_locale: 'en-us',
// 	_include_references: true
// }, {}).then((data) => {
// 	console.log(JSON.stringify(data));
// })

// 
// DB.insert(entries[0]).then(() => {
// 	console.log('success');
// }).catch(error => {
// 	console.error(error);
// });
// 
// DB.upsert(upsert_entries[2]).then(() => {
// 	console.log('successfully upserted');
// }).catch(error => {
// 	console.error(error);
// });

DB.remove({
	_locale: 'en-us',
	_content_type: 'amul',
	_uid: 'blt-alpha-one'
}).then(() => {
	console.log('removed successfully');
}).catch(error => {
	console.error(error);
})

// 
// DB.findOne({
// 	_content_type: '_assets',
// 	_uid: assets[3].uid,
// 	_locale: 'en-us'
// }).then((data) => {
// 	console.log('success', JSON.stringify(data));
// }).catch(error => {
// 	console.error(error);
// });

// DB.find({
// 	_content_type: 'amul',
// 	_locale: 'en-us'
// }, {}).then((data) => {
// 	console.log('success', JSON.stringify(data));
// }).catch(error => {
// 	console.error(error);
// });

// DB.findOne({
// 	_content_type: '_content_types',
// 	_uid: 'amul',
// 	_locale: 'en-us'
// }).then((data) => {
// 	console.log(JSON.stringify(data));
// }).catch(error => {
// 	console.error(error);
// })

// upsert().then(() => {
// 	console.log('upsert')
// }).catch(error => {
// 	console.error(error);
// });

// upsert().then(() => {
// 	console.log('insert')
// }).catch(error => {
// 	console.error(error);
// });

// find().then(() => {
// 	console.log('find')
// }).catch(error => {
// 	console.error(error);
// });

// find().then(() => {
// 	console.log('findOne')
// }).catch(error => {
// 	console.error(error);
// });