let _ = require('lodash');

let _assets = [
	{
		_data: {
			uid: "1",
			filename: 'one'
		},
		_uid: "1",
		_content_type: '_asset'
	},
	{
		_data: {
			uid: "1",
			filename: '_one_'
		},
		_uid: "_1_",
		_content_type: '_asset'
	},
	{
		_data: {
			uid: "1",
			filename: '__one__'
		},
		_uid: "__1__",
		_content_type: '_asset'
	},
	{
		_data: {
			uid: "2",
			filename: 'two'
		},
		_uid: "2",
		_content_type: '_asset'
	}
]

let removed_asset = _.remove(_assets, {_uid: "1"});
let common_assets = _.compact(_.map(_assets, {_data: {uid: "1"}}));

console.log(`removed_asset : ${JSON.stringify(removed_asset)}`);
console.log('common_assets : ', common_assets);