const A = require('./a'),
	a = new A(),
	b = require('./b')(A, {contentstack: {api_key: 'contentstack_api_key', access_token: 'contentstack_access_token'}});

a.methodA();
b.methodB();