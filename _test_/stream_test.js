const Parent = require('./_stream_parent'),
	Child = require('./_stream_child');

const child = new Child(Parent);

Parent.listners();