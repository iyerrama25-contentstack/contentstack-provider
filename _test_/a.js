class A {
	constructor () {
		console.log('a');
		this.a = 'i am a';
		return this;
	}
	methodA () {
		console.log('methodA');
	}
}

module.exports = A;