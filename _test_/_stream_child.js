const fs = require('fs'),
	path = require('path');
let child = null;

class Child {
	constructor (parent) {
		if (!child) {
			// this.parent = parent;
			// child = this;
			parent.on('assetDownload', (data, cb) => {
				let fileStream = fs.createWriteStream(path.join(__dirname, data._data.filename));
				parent.emit('startStream', data._data.url, (stream) => {
					stream.on('response', resp => {
						if (resp.statusCode === 200) {
							stream.pipe(fileStream);
							fileStream.on('close', () => {
								return cb({staus: 1});
							}).on('error', error => {
								console.error(error);
							});
						} else {
							console.error('stream response isnt 200');
						}
					});
				});
			});
		}
		return child;
	}
}

module.exports = Child;