const EventEmitter = require('events').EventEmitter,
	provider = require('../');

let instance;

class A extends EventEmitter {
	constructor () {
		super();
		if (!instance)
			instance = this;
		return instance
	}

	publish (data) {
		instance.emit('publish', data, callback => {
			console.info(`Return from callback: ${JSON.stringify(callback)}`);
		});
	}

	downloadAsset (data) {
		instance.emit('downloadAssets', data, callback => {
			console.info(`Return from callback: ${JSON.stringify(callback)}`);
		});
	}
}

let a = new A();
let Provider = new provider(a, {contentstack: {api_key: 'somethign', access_token: 'random'}});

// a.publish({
// 	_uid: 'blt123',
// 	_content_type_uid: 'testContentType',
// 	_locale: 'en-us',
// 	_data: {
// 		uid: 'blt123',
// 		title: 'Test Entry',
// 		number: 1
// 	}
// });

let asset = {
    "asset": {
      "uid": "blt02098004f5b1450c",
      "created_at": "2017-12-02T07:58:05.803Z",
      "updated_at": "2017-12-02T07:58:05.803Z",
      "created_by": "bltd94c186015b52323d9429c90",
      "updated_by": "bltd94c186015b52323d9429c90",
      "content_type": "image/jpeg",
      "file_size": "10245",
      "tags": [],
      "filename": "stinson-respect.jpg",
      "url": "https://images.contentstack.io/v3/assets/blt0a3e0fa8bf2766a9/blt02098004f5b1450c/5a225d0d70c95a863a5370bd/download",
      "is_dir": false,
      "_version": 1,
      "title": "stinson-respect.jpg",
      "_internal_url": "/assets/blt02098004f5b1450c/stinson-respect.jpg"
    },
    "_content_type_uid": "_assets",
    "_uid": "blt02098004f5b1450c",
    "_locale": "en-us"
  };
let asset_rte =   {
    "asset": {
      "uid": "blt02098004f5b1450c",
      "url": "https://images.contentstack.io/v3/assets/blt0a3e0fa8bf2766a9/blt02098004f5b1450c/5a225d0d70c95a863a5370bd/download",
      "download_id": "blt02098004f5b1450c/5a225d0d70c95a863a5370bd/download",
      "filename": "stinson-respect.jpg",
      "_internal_url": "/assets/blt02098004f5b1450c/stinson-respect.jpg"
    },
    "_content_type_uid": "_assets",
    "_uid": "/blt02098004f5b1450c/5a225d0d70c95a863a5370bd/download",
    "_locale": "en-us"
  }


a.downloadAsset(asset_rte);